import sys, json

codes = ''
with open(".play_list", "r+") as readfile:
	data = readfile.read()
	jsonData = json.loads(data)

	for code in jsonData:
		codes += code["code"]+ "\n"

readfile.close()

if codes:
	with open(".play_list", "w+") as writefile:
		writefile.write(codes)
	writefile.close()