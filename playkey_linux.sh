#/bin/bash

playkey_path=$(find $HOME -iname 'playkey.exe' -print -quit)
token=$(cat .token)
if [[ -z $(cat ./.token) ]]; then
	echo -n "You need to set playkey token: "
	read token
	echo $token >> .token
fi
case $1 in
	--set-token)
		> .token
		read -p "Set playkey token: " token
		echo "Your new token: $token"
		echo $token >> .token		
		;;
	list)
		if [[ -z $play_list ]]; then
			if grep "code" .play_list > /dev/null; then
				python get_list.py
				echo echo cat ./.play_list
			else
				> .play_list
				curl -d '{"lang":"ru","filter_codes":null,"filter_categories_and_genres":null,"offset":18,"limit":null,"filter_name":null,"token":"'$token'","ga_clientId":"559644779.1572964704"}' -H "Content-Type: application/json" -X POST https://api.playkey.net/rest/PlaykeyAPI.svc/gameshipments/all/auth | jq -r '.game_shipments_any' >> .play_list
				python get_list.py
				echo cat ./.play_list
			fi
		else
			for line in $play_list
			do
				echo $line
			done
		fi
	;;	
	run)
		if [[ -z $2 ]]; then
			echo "you need set game for launch. For example: playkey_linux.sh run destiny2"
		else
			> ./.play_url
			echo 'selected game is: "'$2'"'
			curl -d '{"lang":"ru","token":"'$token'","code_game":"'$2'","play_config":"Ultra","ga_clientId":"ga don`t initialize"}' -H "Content-Type: application/json" -X POST https://api.playkey.net/rest/PlaykeyAPI.svc/games/play | jq -r '.play_url' >> ./.play_url
			if [[ -n $(cat ./.play_url) && -n $playkey_path && $(cat ./.play_url) != 'null' ]]; then
				echo 'url finded trying to start playkey with wine'
				wine "$playkey_path" "$(cat ./.play_url)"
			else
				echo "Trying to get new url"
				echo curl -d '{"lang":"ru","token":"'$token'","code_game":"'$2'","play_config":"Ultra","ga_clientId":"ga don`t initialize"}' -H "Content-Type: application/json" -X POST https://api.playkey.net/rest/PlaykeyAPI.svc/games/play | jq -r '.play_url' >> ./.play_url
				wine "$playkey_path" "$(cat ./.play_url)"
			fi	
		fi
		;;
	*)
		echo "Look's like you need to set some command. For example: playkey_linux.sh run the_outer_worlds"
		;;
esac	


